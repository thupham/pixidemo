import * as PIXI from "pixi.js";
import Component from "./components/index";
import Game from "./game";

const DEFAULT_OPTIONS = {
  width: 1120,
  height: 630,
  resolution: 2,
  backgroundColor: 0x999999,
  autoResize: true,
  antialias: false,
  transparent: false,
  roundPixels: true
};

let game = null;

export function getGame() {
  return game;
}

function initGame(element, options) {
  options = Object.assign(DEFAULT_OPTIONS, options);
  if (typeof element === 'object') {
    let renderer = PixiUtils.createRenderer(options);
    game = new Game();
    game.options = options;
    game.fetchScenes(element.props.children);
    game.stage = Component.createContainer();
    game.renderer = renderer;
    game.props = element.props;
    game.ticker = new PIXI.ticker.Ticker();
    game.start();
    return game;
  }
}

function createRenderer(options) {
  let renderer = new PIXI.autoDetectRenderer(options);
  renderer.plugins.interaction.autoPreventDefault = false;
  renderer.view.style.touchAction = 'auto';
  return renderer;
}

const PixiUtils = {
  createRenderer: createRenderer,
  initGame: initGame
};

export default PixiUtils;