import * as PIXI from "pixi.js";
let assetsStore = {};

export const ASSET_TYPE = {
  IMAGE: '@img'
};

let resolve = {};
resolve[ASSET_TYPE.IMAGE] = function (params) {
  let r = assetsStore[params[1]];
  if (r) {
    return r.data.texture;
  } else {
    return PIXI.Texture.EMPTY;
  }
};

function resolveAsset(id) {
  let params = id.split('/');
  return resolve[params[0]](params);
}

function addAsset(name, data) {
  let nameData = name.split('/');
  let type = nameData[0];
  let id = nameData[1];
  assetsStore[id] = {type, id, name, data, options: {}};
  if (type === ASSET_TYPE.SPRITE_SHEET) {
    assetsStore[id].options.frameWidth = parseInt(nameData[2], 10);
    assetsStore[id].options.frameHeight = parseInt(nameData[3], 10);
  }
}

const AssetsManager = {
  resolveAsset: resolveAsset,
  addAsset: addAsset
};

export default AssetsManager;