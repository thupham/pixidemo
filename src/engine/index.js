import render from './render';
import {TYPES} from "./components/index";
import Loader from "./loader";
import PixiUtils from "./pixiUtils";
import Game from "./game";
import Scene from "./scene";
import Component from "./component";
import Builder from "./builder";

export const Sprite = TYPES.SPRITE;
export const Text = TYPES.TEXT;
export const Container = TYPES.CONTAINER;
export const Button = TYPES.BUTTON;
export const ParticleContainer = TYPES.PARTICLE_CONTAINER;
export const AdvanceText = TYPES.ADVANCE_TEXT;
export const ParticleEmitter = TYPES.PARTICLES_EMITTER;

const Engine = {
  render: render,
  Loader: Loader,
  changeScene: PixiUtils.changeScene,
  Game: Game,
  Scene: Scene,
  Component: Component,
  createElement: Builder.createElement,
};
export default Engine;