import PixiComponent from "./PixiComponent";
import * as PIXI from "pixi.js";
import AssetsManager from "../assetsManager";

const PERMITTED_PROPS = [
  'alpha',
  'x',
  'y',
  'value',
  'fontFamily',
  'size',
  'align',
  'tint',
  'anchor',
  'imageMap'
];

const METACHARS = /[[\]{}()*+?.\\|^$\-,&#\s]/g;

export default class PixiAdvanceText extends PixiComponent {
  set value(text) {
    this.props.value = text;
    this.updateTextContent();
  }

  set size(size) {
    this.props.size = size;
    this.updateTextContent();
  }

  set tint(color) {
    this.renderer.fill = [color];
  }

  createRenderer() {
    this.renderer = new PIXI.Container();
    this.updateTextContent();
  }

  updateTextContent() {
    this.renderer.removeChildren();
    let parsedText = this.parseText();
    const style = new PIXI.TextStyle({
      fontFamily: "Verdana, Geneva, sans-serif",
      fontSize: this.props.size || 26,
      fill: [
        this.props.tint || 'white'
      ],
    });
    let prevX = 0;
    parsedText.forEach((textData) => {
      let pixiComponent = null;
      if (textData.type === 'text') {
        pixiComponent = new PIXI.Text(textData.value, style);
      } else if (textData.type === 'image') {
        let texture = AssetsManager.resolveAsset(this.props.imageMap[textData.value]);
        pixiComponent = new PIXI.Sprite(texture);
        let scale = style.fontSize / pixiComponent.height;
        pixiComponent.scale.set(scale);
      }
      pixiComponent.x = prevX;
      this.renderer.addChild(pixiComponent);
      prevX = this.renderer.getLocalBounds().width;
      console.log(this.renderer.getLocalBounds());
    });
  }

  createRegex() {
    let patterns = [];
    for (let emoji in this.props.imageMap) {
      patterns.push('(' + emoji.replace(METACHARS, "\\$&") + ')');
    }
    return new RegExp(patterns.join('|'), 'g');
  }

  parseText() {
    let regexp = this.createRegex();
    let text = this.props.value;
    let parsed = [];
    text.replace(regexp, (code) => {
      let splitted = text.split(code);
      text = splitted[1];
      parsed.push({
        type: 'text',
        value: splitted[0]
      });
      parsed.push({
        type: 'image',
        value: code
      })
    });
    parsed.push({
      type: 'text',
      value: text
    });
    return parsed;
  }

  static create(parent, props = {}) {
    let text = new PixiAdvanceText();
    text.setProps(props, PERMITTED_PROPS);
    text.createRenderer();
    text.parent = parent;
    return text;
  }
}