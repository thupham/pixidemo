import PixiSprite from "./PixiSprite";
import PixiText from "./PixiText";
import PixiContainer from "./PixiContainer";
import PixiButton from "./PixiButton";
import PixiParticleContainer from "./PixiParticleContainer";
import PixiAdvanceText from "./PixiAdvanceText";
import PixiParticlesEmitter from "./PixiParticlesEmitter";

export const TYPES = {
  SPRITE: "Sprite",
  TEXT: "Text",
  CONTAINER: "Container",
  BUTTON: "Button",
  PARTICLE_CONTAINER: "ParticleContainer",
  ADVANCE_TEXT: "AdvanceText",
  PARTICLES_EMITTER: "ParticleEmitter"
};

let mappingComponent = {};

mappingComponent[TYPES.SPRITE] = PixiSprite;
mappingComponent[TYPES.TEXT] = PixiText;
mappingComponent[TYPES.CONTAINER] = PixiContainer;
mappingComponent[TYPES.BUTTON] = PixiButton;
mappingComponent[TYPES.PARTICLE_CONTAINER] = PixiParticleContainer;
mappingComponent[TYPES.ADVANCE_TEXT] = PixiAdvanceText;
mappingComponent[TYPES.PARTICLES_EMITTER] = PixiParticlesEmitter;

function create(tag, parent, props) {
  let component = mappingComponent[tag].create(parent, props);
  return component;
}

function isRegistered(tag) {
  return typeof mappingComponent[tag] !== 'undefined';
}

function createContainer(parent, props) {
  return mappingComponent[TYPES.CONTAINER].create(parent, props);
}

const Component = {
  create: create,
  isRegistered: isRegistered,
  createContainer: createContainer
};

export default Component;