import * as PIXI from "pixi.js";
import PixiComponent from "./PixiComponent";
import AssetsManager from "../assetsManager";
import 'pixi-particles';
import {getGame} from "../pixiUtils";

export default class PixiParticlesEmitter extends PixiComponent {

  update(time) {
    this._renderer.update(time * 0.01);
  }

  static create(parent = null, props = {}) {
    let emitter = new PixiParticlesEmitter();
    getGame().tickerEntities.push(emitter);
    let textures = props.listSrc.map((src) => {
      return AssetsManager.resolveAsset(src)
    });
    console.log(textures);
    emitter.renderer = new PIXI.particles.Emitter(
      parent._renderer,
      textures,
      props.options
    );
    return emitter;
  }
}