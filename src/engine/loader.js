import AssetsManager, {ASSET_TYPE} from "./assetsManager";
import * as PIXI from "pixi.js";

let addedIds = [];
PIXI.loader.onProgress.add((_, data) => {
  AssetsManager.addAsset(data.name, data);
});

PIXI.loader.onError.add((_, data) => {
  console.warn('Error loading ' + data.name);
});

function addAsset(id, path) {
  if (addedIds.indexOf(id) > -1) return;
  addedIds.push(id);
  return PIXI.loader.add(`${ASSET_TYPE.IMAGE}/${id}`, path);
}

function load() {
  return PIXI.loader.load();
}

function addCompleteCallback(fn) {
  PIXI.loader.onComplete.add(fn);
}

function reset() {
  PIXI.loader.reset();
  PIXI.loader.onComplete.detachAll();
}

const Loader = {
  progress: PIXI.loader.progress,
  addCompleteCallback,
  addAsset,
  load,
  reset,
};

export default Loader;