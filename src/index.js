import React from 'react';
import './index.css';
import Engine from './engine';
import App from './scene/App';
import Load from "./scene/Load";
import LayerDemo from "./scene/LayerDemo";
import TextDemo from "./scene/TextDemo";
import ParticleDemo from "./scene/ParticleDemo";

Engine.render((
  <Engine.Game>
    <Engine.Scene name={'load'} component={Load} default={true}/>
    <Engine.Scene name={'menu'} component={App}/>
    <Engine.Scene name={'layer'} component={LayerDemo}/>
    <Engine.Scene name={'text'} component={TextDemo}/>
    <Engine.Scene name={'particle'} component={ParticleDemo}/>
  </Engine.Game>
), document.getElementById('app'));
