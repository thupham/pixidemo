import React from 'react';
import Engine, {Container, Text} from "../engine/index";
import {AdvanceText} from "../engine";
import {getGame} from "../engine/pixiUtils";

const rangeRandom = (from, to) => {
  return Math.round(Math.random() * to + from);
};

const EMOJI_DICTIONARY = {
  ':haha:': '@img/haha',
  ':wow:': '@img/wow'
};

const TEXT_FOR_TESTING = [
  'test :haha: test :haha:',
  ':haha: test :wow: test',
  'test test test',
  ':haha: :wow: :hola:',
  'test :haha:',
  ':haha::wow::haha:'
];

class TextDemo extends Engine.Component {

  start() {
    this.interval = setInterval(() => {
      let randomTextIdx = rangeRandom(0, TEXT_FOR_TESTING.length - 1);
      let randomSize = rangeRandom(1, 100);
      this.refs['text'].component.size = randomSize;
      this.refs['text'].component.value = TEXT_FOR_TESTING[randomTextIdx];
    }, 2000);
  }

  exit() {
    clearInterval(this.interval);
  }

  update() {
    this.refs['fps'].component.value = 'FPS: ' + getGame().ticker.FPS;
  }

  render() {
    return (
      <Container>
        <AdvanceText value={TEXT_FOR_TESTING[3]} tint={'0x000000'} align={'center'} size={100}
                     ref={'text'} x={100} y={100} imageMap={EMOJI_DICTIONARY}/>
        <Container x={300}>
          <Text value={'fps'} ref={'fps'} tint={0x000000}/>
        </Container>
      </Container>
    );
  }
}

export default TextDemo;
