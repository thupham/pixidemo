import React from 'react';
import Engine, {Container} from "../engine/index";

class Load extends Engine.Component {
  start() {
    Engine.Loader.addCompleteCallback(this.completeLoading.bind(this));
    Engine.Loader.addAsset(`card`, require(`../assets/card.jpg`));
    Engine.Loader.addAsset(`menu`, require(`../assets/menu.jpg`));
    Engine.Loader.addAsset(`haha`, require(`../assets/haha.png`));
    Engine.Loader.addAsset(`wow`, require(`../assets/wow.png`));

    Engine.Loader.addAsset(`flame-1`, require(`../assets/particles/flame_01.png`));
    Engine.Loader.addAsset(`flame-2`, require(`../assets/particles/flame_02.png`));
    Engine.Loader.addAsset(`flame-3`, require(`../assets/particles/flame_03.png`));
    Engine.Loader.addAsset(`flame-4`, require(`../assets/particles/flame_04.png`));
    Engine.Loader.addAsset(`flame-5`, require(`../assets/particles/flame_05.png`));
    Engine.Loader.addAsset(`flame-6`, require(`../assets/particles/flame_06.png`));


    Engine.Loader.load();
  }

  completeLoading() {
    Engine.Loader.reset();
    this.game.changeScene('menu');
  }

  render() {
    return (
      <Container>

      </Container>
    );
  }
}

export default Load;
