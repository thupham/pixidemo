import React from 'react';
import Engine, {Container, Text, ParticleEmitter} from "../engine";
import {getGame} from "../engine/pixiUtils";

const DARK_FIRE_PARTICLE_OPTIONS = {
  "alpha": {
    "start": 0.69,
    "end": 0.25
  },
  "scale": {
    "start": 0.1,
    "end": 0.2,
    "minimumScaleMultiplier": 1
  },
  "color": {
    "start": "#1c1006",
    "end": "#ff0400"
  },
  "speed": {
    "start": 500,
    "end": 500,
    "minimumSpeedMultiplier": 0.2
  },
  "acceleration": {
    "x": -20,
    "y": 0
  },
  "maxSpeed": 0,
  "startRotation": {
    "min": 265,
    "max": 275
  },
  "noRotation": false,
  "rotationSpeed": {
    "min": 50,
    "max": 50
  },
  "lifetime": {
    "min": 0.05,
    "max": 0.5
  },
  "blendMode": "normal",
  "frequency": 0.001,
  "emitterLifetime": -0.99,
  "maxParticles": 50,
  "pos": {
    "x": 0,
    "y": 0
  },
  "addAtBack": false,
  "spawnType": "circle",
  "spawnCircle": {
    "x": 0,
    "y": 0,
    "r": 10
  }
};

const FIRE_PARTICLE_OPTIONS = {
  "alpha": {
    "start": 0.69,
    "end": 0.25
  },
  "scale": {
    "start": 0.1,
    "end": 0.2,
    "minimumScaleMultiplier": 1
  },
  "color": {
    "start": "#ffffff",
    "end": "#ff0400"
  },
  "speed": {
    "start": 500,
    "end": 500,
    "minimumSpeedMultiplier": 0.2
  },
  "acceleration": {
    "x": -20,
    "y": 0
  },
  "maxSpeed": 0,
  "startRotation": {
    "min": 265,
    "max": 275
  },
  "noRotation": false,
  "rotationSpeed": {
    "min": 50,
    "max": 50
  },
  "lifetime": {
    "min": 0.05,
    "max": 0.5
  },
  "blendMode": "normal",
  "frequency": 0.001,
  "emitterLifetime": -0.99,
  "maxParticles": 10,
  "pos": {
    "x": 0,
    "y": 0
  },
  "addAtBack": false,
  "spawnType": "circle",
  "spawnCircle": {
    "x": 0,
    "y": 0,
    "r": 10
  }
};

const PARTICLE_IMAGES = ['@img/flame-1', '@img/flame-2', '@img/flame-3', '@img/flame-4', '@img/flame-5', '@img/flame-6'];


class ParticleDemo extends Engine.Component {
  render() {
    return (
      <Container>
        <Container x={300} y={300}>
          <ParticleEmitter listSrc={PARTICLE_IMAGES} options={FIRE_PARTICLE_OPTIONS}/>
        </Container>

        <Container x={700} y={300}>
          <ParticleEmitter listSrc={PARTICLE_IMAGES} options={DARK_FIRE_PARTICLE_OPTIONS}/>
        </Container>
        <Container x={300}>
          <Text value={'fps'} ref={'fps'} tint={0x000000}/>
        </Container>
      </Container>
    );
  }

  update() {
    this.refs['fps'].component.value = 'FPS: ' + getGame().ticker.FPS;
  }
}

export default ParticleDemo;
