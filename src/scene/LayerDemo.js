import React from 'react';
import Engine, {Container, Text} from "../engine/index";
import {ParticleContainer, Sprite} from "../engine";
import * as TWEEN from 'es6-tween';
import {getGame} from "../engine/pixiUtils";

const CARD_NUMS = 144;
const NEW_STACK_DISTANCE_Y = 400;
const ANIMATION_TIME = 2000;

class LayerDemo extends Engine.Component {
  renderFirstStack() {
    let components = [];
    for (let i = 0; i < CARD_NUMS; i++) {
      components.push(<Sprite src={'@img/card'} x={i * 3} y={i * 3} ref={`card${i}`} key={`card${i}`}/>)
    }
    return components;
  }

  start() {
    for (let i = CARD_NUMS - 1; i >= 0; i--) {
      let cardPosition = this.refs[`card${i}`].component.position;
      new TWEEN.Tween({x: cardPosition.x})
        .delay((CARD_NUMS - i) * ANIMATION_TIME)
        .to({x: cardPosition.x + NEW_STACK_DISTANCE_Y}, ANIMATION_TIME)
        .on('update', ({x}) => {
          this.refs[`card${i}`].component.x = x;
        })
        .on('complete', () => {
          this.refs[`container`].component.changeLayer(this.refs[`card${i}`].component, CARD_NUMS - i)
        })
        .start();
    }
  }

  update() {
    this.refs['fps'].component.value = 'FPS: ' + getGame().ticker.FPS;
  }

  render() {
    return (
      <Container>
        <ParticleContainer ref={'container'}>
          {this.renderFirstStack()}
        </ParticleContainer>
        <Container x={300}>
          <Text value={'fps'} ref={'fps'} tint={0x000000}/>
        </Container>
      </Container>
    );
  }
}

export default LayerDemo;
