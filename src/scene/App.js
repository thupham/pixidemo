import React from 'react';
import Engine, {Button, Container, Text} from "../engine/index";

class App extends Engine.Component {
  constructor(game, props) {
    super(game, props);
    this.navigateToLayerDemo = this.navigateToLayerDemo.bind(this);
    this.navigateToTextDemo = this.navigateToTextDemo.bind(this);
    this.navigateToParticleDemo = this.navigateToParticleDemo.bind(this);
  }

  navigateToTextDemo() {
    this.game.changeScene('text');
  }

  navigateToParticleDemo() {
    this.game.changeScene('particle');
  }

  navigateToLayerDemo() {
    this.game.changeScene('layer');
  }

  render() {
    return (
      <Container>
        <Container x={100} y={100}>
          <Button src={'@img/menu'} onClick={this.navigateToLayerDemo}/>
          <Text value={'Layer'} tint={'0x000000'} align={'center'} x={80} y={25}/>
        </Container>
        <Container x={100} y={250}>
          <Button src={'@img/menu'} onClick={this.navigateToTextDemo}/>
          <Text value={'Text'} tint={'0x000000'} align={'center'} x={80} y={25}/>
        </Container>
        <Container x={100} y={400}>
          <Button src={'@img/menu'} onClick={this.navigateToParticleDemo}/>
          <Text value={'Particles'} tint={'0x000000'} align={'center'} x={80} y={25}/>
        </Container>
      </Container>
    );
  }
}

export default App;
